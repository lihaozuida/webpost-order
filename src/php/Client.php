#!/usr/bin/env php
<?php
namespace wporder\php;

error_reporting(E_ALL);

require_once __DIR__ . '/ThriftAutoload.php';

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TBufferedTransport;
use Thrift\Transport\TSocket;
use Thrift\Transport\THttpClient;

use \wpweb\WepostWebServiceClient;

if (array_search('--http', $argv)) {
    $socket = new THttpClient('r.wepost.cc', 8080, '/php/WepostWebServiceServer.php');
} else {
    $socket = new TSocket('localhost', 9090);
}
$transport = new TBufferedTransport($socket, 1024, 1024);
$protocol = new TBinaryProtocol($transport);
$client = new WepostWebServiceClient($protocol);

$transport->open();

$resourceId = $client->addResource('xiaoyang', 1);

var_dump($resourceId);

$resources = $client->getResources(1, time(), 1, 10);

var_dump($resources);

$transport->close();