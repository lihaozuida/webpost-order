<?php

// Thrift class loader
require_once __DIR__ . '/../../lib/php/lib/Thrift/ClassLoader/ThriftClassLoader.php';

use Thrift\ClassLoader\ThriftClassLoader;

// Thrift
$GEN_DIR = realpath(dirname(__FILE__) . '/..') . '/gen-php';
$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', __DIR__ . '/../../lib/php/lib');
$loader->registerDefinition('wporder', $GEN_DIR);
$loader->register();