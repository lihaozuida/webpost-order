<?php
namespace wporder\php;

require_once __DIR__ . '/ThriftAutoload.php';

require_once __DIR__ . '/dao/bootstrap.php';

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TPhpStream;
use Thrift\Transport\TBufferedTransport;

use wporder\WepostOrderServiceIf;

if (php_sapi_name() == 'cli') {
    ini_set("display_errors", "stderr");
}

class WepostOrderServiceHandler implements WepostOrderServiceIf
{

    /**
     * @param \wporder\Order $order
     * @return \wporder\Response
     */
    public function addOrder(\wporder\Order $order)
    {
        $orderRepository = $GLOBALS['entityManager']->getRepository('WepostOrderEntity\Order');
        $orderRepository->addOrder($order);

        $response = new \wporder\Response();
        $response->responseCode = \wporder\ResponseCode::SUCCESS;
        $response->order = $order;

        return $response;
    }

    /**
     * @param int $orderId
     * @return \wporder\Order
     */
    public function getOrder($orderId)
    {
        $orderRepository = $GLOBALS['entityManager']->getRepository('WepostOrderEntity\Order');
        $order = $orderRepository->getOrder($orderId);

        $response = new \wporder\Response();
        $response->responseCode = \wporder\ResponseCode::SUCCESS;
        $response->order = $order;

        return $response;
    }

    /**
     * @param string $userId
     * @return \wporder\Order[]
     */
    public function getOrders($userId)
    {
        // TODO: Implement getOrders() method.
    }

    /**
     * @param \wporder\Order $order
     * @param int $currentStatus
     * @param int $newStatus
     * @return \wporder\Response
     */
    public function updateOrderPaymentStatus(\wporder\Order $order, $currentStatus, $newStatus)
    {
        $orderRepository = $GLOBALS['entityManager']->getRepository('WepostOrderEntity\Order');
        $orderRepository->updateOrderPaymentStatus($order, $currentStatus, $newStatus);

        $response = new \wporder\Response();
        $response->responseCode = \wporder\ResponseCode::SUCCESS;

        return $response;
    }

    /**
     * @param \wporder\Order $order
     * @param int $currentStatus
     * @param int $newStatus
     * @return \wporder\Response
     */
    public function updateOrderShippingStatus(\wporder\Order $order, $currentStatus, $newStatus)
    {
        $orderRepository = $GLOBALS['entityManager']->getRepository('WepostOrderEntity\Order');
        $orderRepository->updateOrderShippingStatus($order, $currentStatus, $newStatus);

        $response = new \wporder\Response();
        $response->responseCode = \wporder\ResponseCode::SUCCESS;

        return $response;
    }
}

header('Content - Type', 'application / x - thrift');
if (php_sapi_name() == 'cli') {
    echo "\r\n";
}

$handler = new WepostOrderServiceHandler();
$processor = new \wporder\WepostOrderServiceProcessor($handler);

// HTTP
$transport = new TBufferedTransport(new TPhpStream(TPhpStream::MODE_R | TPhpStream::MODE_W));
$protocol = new TBinaryProtocol($transport, true, true);

$transport->open();
$processor->process($protocol, $protocol);
$transport->close();

// TCP
//use Thrift\Server\TForkingServer;
//use Thrift\Server\TServerSocket;
//use Thrift\Factory\TBinaryProtocolFactory;
//use Thrift\Factory\TTransportFactory;

//$serverSocket = new TServerSocket('localhost', 9090);
//$protocolFactory = new TBinaryProtocolFactory(true, true);
//$transportFactory = new TTransportFactory();
//$server = new TForkingServer(
//    $processor,
//    $serverSocket,
//    $transportFactory,
//    $transportFactory,
//    $protocolFactory,
//    $protocolFactory
//);
//$server->serve();