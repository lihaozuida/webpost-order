<?php
namespace WepostOrderEntity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * Class Order
 * @package WepostOrderEntity
 *
 * @Entity(repositoryClass="OrderRepository")
 * @Table(name="t_order")
 */
class Order
{
    /**
     * @Column(name="f_order_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $orderId;

    /**
     * @Column(name="f_user_id", type="string")
     */
    protected $userId;

    /**
     * @Column(name="f_product_id", type="integer")
     */
    protected $productId;

    /**
     * @Column(name="f_title", type="string")
     */
    protected $title;

    /**
     * @Column(name="f_payment", type="integer")
     */
    protected $payment;

    /**
     * @Column(name="f_create_time", type="integer")
     */
    protected $createTime;

    /**
     * @Column(name="f_update_time", type="integer")
     */
    protected $updateTime;

    /**
     * @Column(name="f_payment_time", type="integer")
     */
    protected $paymentTime = 0;

    /**
     * @Column(name="f_payment_status", type="integer")
     */
    protected $paymentStatus = 0;

    /**
     * @Column(name="f_shipping_status", type="integer")
     */
    protected $shippingStatus = 1;

    /**
     * @Column(name="f_extra", type="string")
     */
    protected $extra;

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @param mixed $createTime
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @param mixed $updateTime
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    }

    /**
     * @return mixed
     */
    public function getPaymentTime()
    {
        return $this->paymentTime;
    }

    /**
     * @param mixed $paymentTime
     */
    public function setPaymentTime($paymentTime)
    {
        $this->paymentTime = $paymentTime;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param mixed $paymentStatus
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    /**
     * @return mixed
     */
    public function getShippingStatus()
    {
        return $this->shippingStatus;
    }

    /**
     * @param mixed $shippingStatus
     */
    public function setShippingStatus($shippingStatus)
    {
        $this->shippingStatus = $shippingStatus;
    }

    /**
     * @return mixed
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param mixed $extra
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
    }
}