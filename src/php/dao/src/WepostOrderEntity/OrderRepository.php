<?php
namespace WepostOrderEntity;

use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    public function addOrder(Order $order)
    {
        $em = $this->getEntityManager();

        $em->persist($order);
        $em->flush();

        return $order;
    }

    public function getOrder($orderId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('o')->from('WepostOrderEntity\Order', 'o')->where($qb->expr()->eq('o.orderId', $orderId));
        $qb->getQuery()->getResult();
    }

    public function getOrders($userId)
    {

    }

    public function updateOrderPaymentStatus(Order $order, $oldStatus, $newStatus) {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->update('WepostOrderEntity\Order', 'o')->set('o.paymentStatus', $newStatus)->where(
            $qb->expr()->eq('o.orderId', $order->getOrderId()),
            $qb->expr()->eq('o.paymentStatus', $oldStatus)
        )->getQuery();

        return $query->execute();
    }

    public function updateOrderShippingStatus(Order $order, $oldStatus, $newStatus) {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->update('WepostOrderEntity\Order', 'o')->set('o.shippingStatus', $newStatus)->where(
            $qb->expr()->eq('o.orderId', $order->getOrderId()),
            $qb->expr()->eq('o.shippingStatus', $oldStatus)
        )->getQuery();

        return $query->execute();
    }
}