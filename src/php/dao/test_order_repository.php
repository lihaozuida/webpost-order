<?php
// create_product.php

require_once __DIR__ . '/../ThriftAutoload.php';

require_once "bootstrap.php";


$orderRepository = $entityManager->getRepository('WepostOrderEntity\Order');

$order = new WepostOrderEntity\Order();
$order->setUserId('aaa');
$order->setProductId(1);
$order->setTitle('萝莉乱丢玩具含泪道歉');
$order->setPayment(4999);
$order->setCreateTime(time());
$order->setUpdateTime(time());
$order->setExtra(
    json_encode(
        array('receiver' => 'aa', 'addr' => 'bb', 'street_addr' => 'street_addr', 'phone' => 'phone')
    )
);

$order->setOrderId(1);

//$orderRepository->addOrder($order);

//var_dump(
//    $orderRepository->updateOrderPaymentStatus(
//        $order,
//        \wporder\PaymentStatus::WAIT_PAY,
//        \wporder\PaymentStatus::PAY_SUCCESS
//    )
//);

var_dump(
    $orderRepository->updateOrderShippingStatus(
        $order,
        \wporder\ShippingStatus::WAIT_SHIPPING,
        \wporder\ShippingStatus::SHIPPING
    )
);