<?php
// bootstrap.php

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\ClassLoader\ClassLoader;

require_once __DIR__ . "/../vendor/autoload.php";

// Set dao class position
$classLoader = new ClassLoader();
$classLoader->addPrefix('WepostOrderEntity', __DIR__ . '/src');
$classLoader->register();

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src"), $isDevMode);

// database configuration parameters
$connectionParams = array(
    'dbname' => 'wepost_order',
    'user' => 'root',
    'password' => 'root',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
    'charset' => 'utf8'
);
$conn = DriverManager::getConnection($connectionParams);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);