namespace php wporder

enum PaymentStatus {
    WAIT_PAY = 0,
    PAY_SUCCESS = 1,
    PAY_FAILED = 2,
    REFUND_SUCCESS = 3,
    REFUND_FAILED = 4,
}

enum ShippingStatus {
    WAIT_SHIPPING = 1,
    SHIPPING = 2,
}

enum ResponseCode {
    SUCCESS = 0,
    FAILED = 1,
}

struct Order {
    1: i32 orderId,
    2: string userId,
    3: i32 productId,
    4: string title,
    5: i32 payment,
    6: i32 createTime,
    7: i32 updateTime,
    8: i32 paymentTime,
    9: i32 paymentStatus,
    10: i32 shippingStatus,
    11: string extra,
}

struct Response {
    1: ResponseCode responseCode,
    2: string responseDesc,
    3: Order order,
}

service WepostOrderService {
    Response addOrder(1:Order order),
    Order getOrder(1:i32 orderId),
    list<Order> getOrders(1:string userId),
    Response updateOrderPaymentStatus(1:Order order, 2:PaymentStatus currentStatus, 3:PaymentStatus newStatus),
    Response updateOrderShippingStatus(1:Order order, 2:ShippingStatus currentStatus, 3:ShippingStatus newStatus),
}