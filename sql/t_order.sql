-- phpMyAdmin SQL Dump
-- version 4.3.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2015 at 09:44 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wepost_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE IF NOT EXISTS `t_order` (
  `f_order_id` int(11) unsigned NOT NULL,
  `f_user_id` varchar(64) NOT NULL COMMENT '用户标示，可以是邮箱',
  `f_product_id` int(11) unsigned NOT NULL COMMENT '业务方产品id',
  `f_title` varchar(64) NOT NULL COMMENT '订单标题',
  `f_payment` int(11) unsigned NOT NULL COMMENT '支付金额，单位：分',
  `f_create_time` int(11) unsigned NOT NULL COMMENT '订单创建时间',
  `f_update_time` int(11) unsigned NOT NULL COMMENT '订单更新时间',
  `f_payment_time` int(11) unsigned DEFAULT NULL COMMENT '支付时间',
  `f_payment_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '支付状态 0：待支付 1：支付成功 2：支付失败 3：退款成功 4：退款失败',
  `f_shipping_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '发货状态 1：待发货 2：已发货 ',
  `f_extra` varchar(255) NOT NULL COMMENT '存储冗余信息 例如：给朋友的话'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`f_order_id`, `f_user_id`, `f_product_id`, `f_title`, `f_payment`, `f_create_time`, `f_update_time`, `f_payment_time`, `f_payment_status`, `f_shipping_status`, `f_extra`) VALUES
(1, 'aaa', 1, '萝莉乱丢玩具含泪道歉', 4999, 1447602395, 1447602395, 0, 1, 2, '{"receiver":"aa","addr":"bb","street_addr":"street_addr","phone":"phone"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`f_order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `f_order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
